const sumar = require("../index");
const assert = require("assert");

// Afirmacion
describe("Probar la suma de dos numeros", ()=>{
    // Afirmar que cinco mas cinco es diez
    it('Cinco mas cinco es diez', ()=>{
        assert.equal(10, sumar(5, 5));
    });

    // Afirmar que cinco mas cinco no son cincuenta y cinco
    it("Afirmar que cinco mas cinco no son cincuenta y cinco", ()=>{
        assert.notEqual("55", sumar(5, 5));
    });
});