const log4js = require("log4js");

let logger = log4js.getLogger();

logger.level = "debug";

logger.warn("Cuidado falta el archivo de configuracion");
logger.info("La aplicacion inicio con exito");
logger.error("No se encontro la funcion enviar orden");
logger.fatal("No se pudo iniciar la aplicacion");

function sumar(x, y){
    return x + y;
}

let x = () => 0;

module.exports = sumar;